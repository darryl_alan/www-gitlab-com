describe Gitlab::Homepage::Team::Project do
  describe '.all!' do
    subject { described_class.all! }

    it 'correctly integrates with data/projects.yml' do
      expect(subject).not_to be_empty
      expect(subject).to all(be_a described_class)

      expect(subject.first.name).to eq 'GitLab Community Edition (CE)'
    end
  end

  describe '#mirrors' do
    context 'when project has mirrors' do
      subject do
        described_class.new('gitlab-ce', { 'mirrors' => %w[mirror] })
      end

      it 'returns an array' do
        expect(subject.mirrors).not_to be_empty
      end
    end

    context 'when project does not have mirrors' do
      subject do
        described_class.new('gitlab-ce', { 'name' => 'GitLab CE' })
      end

      it 'returns an empty array' do
        expect(subject.mirrors).to be_empty
      end
    end
  end
end
