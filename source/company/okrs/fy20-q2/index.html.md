---
layout: markdown_page
title: "FY20-Q2 OKRs"
---

This fiscal quarter will run from May 1, 2019 to July 31, 2019.

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV. Grow website visitors. Introduce a certification program. Add a DevOps Transformation offering.

1. CRO: Create “what it looks like to partner with GitLab over the next 3 years” pitch for customers, present and test with at least 5 large enterprise customers.  (Map, Milestones, Outcomes)
1. CRO: Add a Devops Transformation Offering (I agree we want to test this but feel we are setting up a failed OKR unnecessarily by planing to create and deploy this in the next 90-ish days.)  We need to get the foundation in PS and CS set and this will be built with and on top of it.
1. CRO: Introduce a certification program.  This is already an objective for the training and enablement leader when they start.  Assigning it now, just before they are hired is not the right prioritization. (I think we are setting ourselves up with too broad of an objective at the wrong time on this one)
1. CPO
1. CMO:
    1. Open a communication channel with Gitlab.com users. Implement a growth analytics and user communication tool in GitLab (self-managed & gitlab.com), Test an in-app message and hyperlink in Gitlab.com as an iteration, Redesign the gitlab.com/explore page.
       1. Digital Marketing Programs: Redesign gitlab.com/explore page, create better in-product linking to about.gitlab.com, and create a strategy for in-app messaging to increase traffic from product to about.gitlab.com by 10K users.
       2. Marketing Operations: Open up communication channel with CE users. Re-implement [health check](https://gitlab.com/gitlab-com/marketing/marketing-operations/issues/532).
    2. Re-focus on technical & how-to content.  Hire technical content re-mixer, XX number of blog and social technical content, SEO analysis of docs.
       1. PMM: Create at least 1 “how-to” technical content for each hidden IT group (5 total).
       2. Corp Marketing: Engage with technical developers and DevOps audiences via content pillars. Hire technical content editor, work cross-functionally with digital marketing to optimize all new technical content and 10 hardest-working existing technical content pieces, increase new users to the blog through technical and DevOps focused content by 10% Q2 over Q1.
       3. Content Team: Increase publishing cadance. Implement Path Factory. Create 3 content pillars.
       4. Digital Marketing Programs: Drive traffic to docs and from docs to about.gitlab.com via on-page optimization, improved cookie-ing, remarketing, and ABM to increase new user traffic to docs by 10% QoQ.
       5. Field Marketing: Implement ABM Strategy. Increase the number of contacts engaged with us at enterprise named accounts to XXX in XXX accounts.
    3. Attract net-new developers and DevOps practitioners.  Deploy paid digital campaign focused on technical audience, enable retargeting of deeper technical content to docs visitors, launch GitLab "heroes" program, sign-up 1,500 users to 3 user conferences (500 each).
       1. PMM: Define and deliver a series of 5 webinars talks focused on DevOps practitioners, and aligned to campaigns. Tag team with SDRs to drive webinar attendance.
       2. Digital Marketing programs: Build out developer marketing paid digital program to target and attract developers and and to increase form fills for  the demo and increase .com users by 10% QoQ
       3. Corp Marketing: Prominent presence at strategic industry events (KubeCon EU, DOES EMEA, OSCON). Capturing over 15% of an engaged audience (through talks, social, booth scans, giveaways, dinners and meetings)
       4. Technical Evangelism: Reach 10,000 people through technical evangelism efforts (such as talks, onboarding CNCF projects on GitLab CI), create 5 technical blog posts and social technical content from show content within 2 weeks of event.
       5. Technical Evangelism: Prominent presence on (at least 2 net new) foundation committees - CNCF board, CI working group, Helm Summit, KubeCon, Colocated events (and content to show that off)
       6. Events: Announce user conferences. Drive 600 sign ups to the 2 events, align GitLab “heros” program with the user conferences, recruit 5 well-known speakers to be part of the event to drive sign ups. 
       7. Corporate Marketing: Create GitLab brand swell. Launch Onion Labs video and reach 2 MM page visits, GitLab included in 15% of Atlassian and Azure DevOps news moving forward, and increase coverage in EMEA by 10%.
       8. Field Marketing: Execution of 3 technical workshop to build framework for workshops in future qtrs. AWS Summit Excellence. Building audience of 4000 leads to support the practitioner campaign, pipeline generation of 2X investment in Q2 (knowing 5X will be forthcoming in Q3 & Q4).
1. VP Product: [Become more customer driven](https://gitlab.com/gitlab-com/Product/issues/142). Each PM conduct three [customer discovery conversations](/handbook/product/#customer-meetings) with prospects per quarter
1. VP Eng: [Ensure self-managed GitLab scales for large enterprise customers](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4334).
    1. Development
        1. Growth: Increase Revenue. Raise retention metric from Xr to Yr, raise activation metrics from Xact to Yact, raise upsell metric from Xu to Yu, raise adoption metric from Xadp to Yadp.
1. VP Alliances: Close xxx deals through partner custom deals. Joint case study with customer highlighting the secure stage. Secure speaking slots at VMworld (US and EMEA) and ReInvent
1. VP Alliances: deliver shared partner strategy together with the new VP Channels

### CEO: Popular next generation product. Grow use of stages (SMAU). Deliver on maturity and new categories. Add consumption pricing for compute.

1. CRO: Easy to buy new, renewal and growth licenses. Redesign purchase experience from scratch, enable quick, easy and reliable license purchases, additions, and renewals, enable customers, partners and GitLab team to use the same system to quote and take orders and view/manage account.
1. CPO:
1. CMO
    1. Ensure appropriate transactional business/pricing. Analyze buying patterns of Starter & Bronze (user count, etc.), analyze web direct business, develop an iteration to low-end offerings based on data and solving potential conflicts with enterprise selling motions.
    1. Publish visual proof of benefits of single application. Benchmark toolchain clicks, handoffs and integrations, create methodology for comparing to GitLab.
       1. PMM:  Identify 2 customers to work with and create internal business case justification for GitLab purchase based on single application benefits. Working with 2 customers, create and publish MVC 1 for visual proofs of benefits of a single application.
1. VP Product: Grow use of GitLab for all stages of the DevOps lifecycle. Increase Stage Monthly Active Users ([SMAU](/handbook/product/growth/#smau)) 10% m/m for each stage, 3 reference customers using all stages concurrently.
    1. Director of Product, Growth: Improve key growth metrics for both self-managed and GitLab.com. Create MAU dashboard, execute and analyze 4 growth experiments, increase MAU 10% m/m.
    1. Director of Product, Dev: Grow use of GitLab. Increase SMAU 10% m/m for Manage, Plan, Create.
    1. Director of Product, CI/CD: Grow use of GitLab. Increase SMAU 10% m/m for Verify, Package, Release, drive MAU for Verify by 8% through [Grand Unified Theory of CI/CD (high ROI primitives)](/direction/cicd/#powerful-integrated-primitives), drive MAU for Release by 8% through establishing [Progressive Delivery](/direction/cicd/#progressive-delivery) leadership in marketplace and making associated deliverables.
    1. Director of Product, Ops: Grow use of GitLab. Increase SMAU 10% m/m for Monitor, Configure.
    1. Director of Product, Secure: Grow use of GitLab. Increase SMAU 10% m/m for Secure.
    1. Director of Product, Enablement: Ensure Usage Ping is not a flaming piece of garbage. Track more kinds of things, track more robustly.
1. VP Product: Continually improve the maturity of product categories. Deliver on our maturity plan.
    1. Director of Product, Dev: X from `none` to `minimal`, Y from `minimal` to `viable`, Z from `viable` to `complete`.
    1. Director of Product, CI/CD: Move one category each from Verify, and Release, and two from Package, from minimal to complete viability.
    1. Director of Product, Ops: X from `none` to `minimal`, Y from `minimal` to `viable`, Z from `viable` to `complete`.
    1. Director of Product, Secure: X from `none` to `minimal`, Y from `minimal` to `viable`, Z from `viable` to `complete`.
1. VP Product: Sell more CI runner minutes. Add consumption pricing for macOS runners on GitLab.com, add more types of Linux consumption pricing (e.g. larger instances), make add-on CI minutes available to self-managed instances, sell additional $500k in CI runner minutes.
1. VP Eng: [Build our product vision](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4335).
    1. Development: Increase productivity. Raise monthly throughput from ~1800 MRs to ~2160 - [https://gitlab.com/gitlab-com/www-gitlab-com/issues/4307](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4307).
        1. Dev: Increase productivity. Raise monthly throughput from ~420 MRs to ~525 MRs.[https://gitlab.com/gitlab-com/www-gitlab-com/issues/4326](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4326) 
            1. Manage frontend + backend: Increase productivity. Raise monthly throughput from ~150 MRs to ~190 MRs.
            1. Plan frontend + backend: Increase productivity. Raise monthly throughput from ~130 MRs to ~160 MRs.
            1. Create frontend + backend: Increase productivity. Raise monthly throughput from ~140 MRs to ~175 MRs.
        1. CI/CD: Increase productivity. Raise throughput by 20% from Q1 benchmark.
        1. Ops: Increase productivity. Raise throughput by 20% from Q1 benchmark.
        1. Secure: Increase productivity. Raise throughput by 20% from Q1 benchmark.
        1. Enablement: Enablement: [Increase productivity](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4309). Raise throughput by 20% from Q1 benchmark.
    1. Infrastructure: Make all user-visible services ready for mission critical workloads. Storage node migration to ZFS. Deliver Kubernetes. Deliver framework for cost management.
    1. Infrastructure: Control gitlab.com cost. Decrease cost per user from X to Y, Lower total target spend from X to Y.
    1. Quality: Make self-managed mission critical. All new bugs groomed in X timeframe and scheduled according to severity, all existing S1 S2 bugs scheduled successfully with priority and milestone, improve defect grooming visibility for engineering teams. Add 3 metrics into each group's dashboard view, average time to resolve bugs by severity, average time to schedule bugs, missed defect SLA. Provide missed defect SLA report for each group.
    1. Quality: Increase Engineering productivity. Improve defect detection feedback for engineering, cut down end-to-end test runtime in review apps by 50%, cut down review app deployment by 20%.
    1. Security: Secure the company, secure the product and customer data.
    1. Security: Continue rollout of Zero Trust model.
        1. Application Security: HackerOne program spend on plan.
        1. Application Security: Roll out external secure coding training for all developers.
        1. Compliance: Develop and publish Information Security Compliance Roadmap.
        1. Compliance: Complete information security controls gap analysis.
        1. Red Team: Conduct and document  at least 5 threat modeling exercises.
        1. Red Team: Identify 5 risks to customer data in GitLab.com and/or GitLab CE.
        1. Security Operations: Reduce GitLab.com cloud spend through Abuse activity mitigations.
        1. Security Operations: Automate end-to-end vulnerability management process.
    1. Support: Improve the Customer Experience. Build case deflection into support portal, increase docs linked to tickets to 75% (from 51%)
        1. AMER-W: Improve the customer experience by decreasing the time to resolution (TTR) of tickets and reported issues. Develop a TTR metric for resolving tickets. Develop a TTR metric for resolving for S1/S2 issues that arise from support tickets.
        1. Support Operations Specialist: Clean up SFDC -> Zendesk data sync to ensure proper SLA workflows. Consistent sync of relevant contact and Support plan data to Zendesk, documented method to measure data inconsistency errors, SLA assignment in Zendesk consistent with Support plan.
        1. Support Operations Specialist: Create plan to evolve our Customer Satisfaction survey process. Documented changes to CSAT form and frequency of surveying, iterative roll-out plan communicated to relevant teams and customers, document list of key reports to modify or replace.
   
       
    1. Support: Establish consistent escalation path of customers issues. Streamlined responsiveness to customer needs, consistent internal experience, developer happiness.
        1. AMER-E: Define roles & responsibilities between tech support and customer success based on Denver Customer Success Workshop.
        1. AMER-W: Rollout Support Stable Counterparts. 100% of Engineering sub-departments have a Support Manager counterpart. 100% of DevOps Stages have a Support counterpart. Identify and assign counterparts to other departments (e.g. Docs)
        1. Support Operations Specialist: Complete Federal support instance process and implementation.  Document workflow in handbook, complete testing with Security, roll out implementation guidelines to Federal team for customers.
        1. APAC: Establish consistent escalation path of customers' issues. Define escalation triggers and levels. Implement process across Support.
        1. APAC: Iterate on Incident Management for self-managed customers. All Incidents follow the process.
        1. APAC: Improve quality of non-English ticket support. Targeted languages are fully integrated into translation system. Customers experience no friction when having ticket exchanges with Support.
    1. UX: Improve usability. Raise System Usability Score (SUS) from ~74.6 to 76.2.
    1. UX: Great product. Pajamas design system, Address color contrast issues, user onboarding, convert GitLab.com prospects into users.
1. VP Alliances: deliver mature product that can license, bill and meter for our partner offering/marketplaces

### CEO: Great team. Become known as the company for all remote. Get all KPIs and OKRs from dashboards instead of slides. Handbook more accessible and SSoT. Become a world-class hiring and on-boarding company.

1. CFO: Improve accounting processes to support growth and public company reporting requirements.
    1. Controller: Audit completed by July 31, 2019
    1. Senior Technical Accounting Manager: 606 signed off by E&Y prior to end of April.  Other technical accounting issues for FY 18 resolved prior to end of May.
    1. Senior Accounting Manager: Achieve ten day close.
    1. Senior Internal Audit Manager: First phase of SoX compliance completed
        1. Standard Operating Procedures fully documented and included in handbook
        1. Test of Design of internal controls over financial reporting.
        1. Phase I - Test of operating effectiveness completed (For controls which passed the test of design)
1. CFO: Improve financial planning  processes to support growth and increased transparency.
    1. Dir. of Bus Ops: 100% of department dashboards and metrics pages completed with goals and definitions.
    1. Dir. of Bus Ops: BambooHR and Greenhouse ingestion fully automated.
    1. Dir. of Bus Ops:  Historical reporting from snapshots / log in place for SFDC.
    1. Dir. of Bus Ops: Data Quality Process (DQP) completed for ARR, Net and Gross Retention and Customer counts. Third party review completed for same metrics. https://docs.google.com/spreadsheets/d/1qcFD5UkOoG1P2pTfSEUvOkc9zk_vhqCDQR-oVkcB2zY/edit?usp=sharing
    1. FinOps: Fully integrated five year financial model that has gearing ratios documented in the handbook for non-headcount spending and headcount.
    1. FinOps: Metrics incorporated in financial model shown in periscope as KPIs with plan, forecast and actual data.
    1. FinOps: Financial results shown as plan vs actuals vs (forecast-tbd) at department level.
    1. Finance Business Partner: Show working model end to end for integrated financial modeling for support by May 15.
    1. Finance Business Partner:
        1. GitLab.com allocation model signed off by CFO and VP Engineering with handbook updated for methodology.
        1. Product Line P&L completed and issued on a monthly basis.
1. CFO: Create scalable infrastructure for achieving efficient growth
    1. VP of Legal, Contracts, IP and Compliance: 95% of team members covered by scalable employment solution
    1. Director of Business Operations: Procurement function created with leader in place.
    1. Director of Business Operations: First iteration of purchasing process changes merged into handbook.
    1. Director of Business Operations: GitLab Team Members are securely online Day 1
    1. Director of Business Operations: 100% Gitlabber Adoption of OKTA
    1. Director of Business Operations: OSX & Linux standards for purchasing are fully documented and the process is automated where available
        1. Less supported geos have purchasing guidelines documented
        1. Global Repair and Laptop Disposal policies documented
        1. Automate Laptop Deployment via Apple DEP
    1. Director of Business Operations: Evaluate and Install Client side security and provisioning on 50% of GitLab owned laptop fleet.
1. CPO:
    1. D&I / L&D: Enable an environment where all team members feel they belong and are engaged
        1. Defined key business metrics that should be improved by the training.
        1. Implement and iterate on new manager enablement program
        1. Develop and rollout professional communication training
        1. Onboard and enable our Diversity & Inclusion Partner
        1. Implement a diversity dashboard by department
    1. Compensation: Continue the evolution of our Compensation Model
        1. Ramp new Analyst successfully to understand our compensation model, working transparently in GitLab, and ensure proficiency in all related tools.
        1. Complete Phase 1 and 2 of our compensation iterations.
    1. Benefits: Commence a review of benefits in our team member locations
        1. Review Benefits for each payroll provider (entity and PEO) we have and ensure alignment to the global benefits guiding principles.
    1. People Operations: Continue to increase efficiencies across the People Ops team
        1. Transition to using Boards for projects to increase efficiency and transparency
        1. Fill open vacancies in the People Ops team - HRBP x2, People Ops Specialist, Web Content Manager
        1. Iterate further to streamline the Onboarding Issue to enhance our new hire experience
        1. Continue to support the conversion of contractors to our PEO - 10 countries during Q2
        1. Sign off on new HRIS system and have agreed implementation plan
    1. Recruiting: Deliver on the aggressive Hiring Plan in partnership with Leaders
    1. Employment Brand: Evolve GitLabs Employment Brand
        1. Collaborate with Marketing to create and publish a new GitLab recruiting video to promote working at GitLab and what it’s like to work at GItLab..
        1. Define and publish GitLab's employer value proposition.
        1. Identify 5 locations to focus our employment branding, recruitment marketing, and sourcing efforts and create campaign plans for these locations.
        1. Achieve "OpenCompany" status on Glassdoor; create and activate a process for gathering and responding to at least 50% of our Glassdoor reviews.
    1. Talent Operations: Enhance reporting capabilities
        1. Create scorecards for Recruiter, Sourcer, and Coordinator to measure effectiveness and balance workloads.
        1. Partner with Diversity Manager to Improve Diversity reporting capabilities and use it to influence recruiting processes and workflows to better eliminate bias.
        1. Build out leading indicators and evolve progress vs. plan dashboard
    1. Recruiting: Optimize current technology
        1. Evaluate ATS to renew/modify offerings with contract renewal in June.
        1. Consider Greenhouse Inclusion feature or other tools to remove bias.
        1. Find a solution to move from wet signature to electronic signatures for contracts in Germany
    1. Recruiting: Continue to identify process improvement opportunities and improve key metrics:
        1. Further leverage candidate survey data to inform process improvement opportunities.
        1. Initiate Hiring Manager Survey and action key insights.
        1. Continue to work towards stretch goal of 30 days for apply to accept metric
        1. Double the percentage of sourced hires from Q1 to Q2
        1. Standardize onboarding/training for new Recruiters, Coordinators, and Sourcers to provide efficient and effective ramp up with consistent ways of working.
        1. Hire executive recruiter to support more senior level roles / pull back on agency reliance
        1. Low location factor hiring goal consistent with department goals
1. CMO:
    1. Launch All-Remote web destination. Digest existing all-remote content, create an all-remote landing page for recruiting support, at least 2 all-remote related press articles in mainstream press (defined as non-tech press).
        1. Corp Marketing: Launch all remote thought leadership platform. Increase coverage of GitLab being all remote by 20% in the media, secure 2 mainstream media articles, increase speaking opportunities on all remote by 20%.
        2. Digital Marketing Programs: Ensure metrics from Google Analytics are accessible in funnel and consult on all top-of-funnel and website metrics reporting discussions
        3. Digital Marketing Programs: Improve inbound lead conversions by creating end-to-end customer nurture paths, with email and digital advertising to increase MQLs.
    1. 100% real-time funnel metrics. All funnel marketing metrics in periscope available to anyone at GitLab.
    1. Achieve Revenue Plan funnel targets. $13m in Net New IACV Pipeline created by beginning of Q3 FY20. 34,000 MQLs created, 11% MQL to SAO conversion rate.
1. VP Product: Evangelize GitLab as the best-place to be a product manager. Contribute 5 product management focused blog posts, Submit 10 CFPs.
1. VP Product: Hire a great team - with recruiting. Hire to 100% of plan, Reduce time to offer to below company average, Increase team diversity by 40%.
1. VP Product: Promote cohesive team. Launch #pm_standup channel with multiple weekly engagements (customer, product, market), Launch weekly Team Pulse survey.
1. VP Product: [Become data-informed](https://gitlab.com/gitlab-com/Product/issues/166). Get All KPIs built (with Business Ops), 50% of direction items ship with usage metrics, 40% of direction items are provably successful.
1. VP Eng: [Scale processes to accommodate a 500 person organization](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4336)
    1. Development: Achieve Level 3/4 Maturity for KPIs.  Key results Level three on Major KPIs listed in [https://gitlab.com/gitlab-com/www-gitlab-com/issues/4313](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4313)
        1. Dev: Build KPI BE Unit test coverage and FE Unit test coverage. [https://gitlab.com/gitlab-com/www-gitlab-com/issues/4329](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4329)
        1. Ops: Roll out the pulse survey to FE team.
        1. Secure: Roll out the pulse survey across the sub-department teams.
        1. Growth: Build KPI X.
    1. Development: Hire to plan. X sourced candidates screened, engagement with security-specific recruiting firm, 42 hires being made. - [https://gitlab.com/gitlab-com/www-gitlab-com/issues/4308](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4308)
        1. Dev: Hire to plan. 3 IC hires and 2 engineering managers made. [https://gitlab.com/gitlab-com/www-gitlab-com/issues/4330](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4330)
            1. Manage frontend: Hire to plan. 1 hire made.
            1. Plan backend: Hire to plan. 1 hire made.
            1. Plan frontend: Hire to plan. 1 hire made.
        1. CI/CD: Hire to plan. 6 hires made.
        1. Ops: Hire to plan. 5 hires made.
        1. Secure: Hire to plan. 5 hires made.
        1. Defend: Hire to plan. 4 hires made.
        1. Growth: Hire to plan. X engaged candidates supplied by hiring manager, X hires made.
        1. Enablement: [Hire to plan](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4269). 100 engaged candidates supplied by hiring manager, 12 hires made.
    1. Infrastructure: Get all KPIs built.
    1. Quality: Get all KPIs built.
    1. Security: Get all KPIs built.
        1. Application Security: Hire to plan. 2 hires made.
        1. Compliance: Hire to plan. 1 hire made.
        1. Red Team: Hire to plan. 2 hires made.
        1. Security Operations: Hire to plan. 2 hires made.
    1. Support: TBD objective. Get all KPIs built, transition staffing model blending productivity and revenue, update hiring plan for remainder of FY20 by region with Recruiting, update team page to reflect changes, iterate on Support roles definitions in Handbook.
        1. AMER-W: Develop blended team model. Build career development plan for agents. Equip agents to be capable of delivering 25% of first responses in self-managed queues.
        1. AMER-E: Convert all current metrics to use FY designation & Explore Periscope or Zendesk Explore as the next iteration platform.
        1. AMER-E: Review Support Engineering Onboarding and redesign to align with product stages and define post-onboarding stages.
    1. UX: Get all KPIs built.
1. VP Alliances: Launch marketing program for filling the acquisitions pipeline. Deliver 2 acquisitions to completion.
