/* global setupCountdown */

function setupHackathonCountdown() {
  var nextHackathonDate = new Date('May 22, 2019 00:00:00').getTime();

  setupCountdown(nextHackathonDate, 'nextHackathonCountdown');
}

(function() {
  setupHackathonCountdown();
})();

