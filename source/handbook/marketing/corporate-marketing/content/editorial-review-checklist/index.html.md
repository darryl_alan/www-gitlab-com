---
layout: markdown_page
title: "Content"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Editorial review checklist

This checklist is primarily intended for use when conducting a final review of posts for the GitLab blog. If you have a query about style or grammar that isn't answered in the [editorial style guidelines](/handbook/marketing/corporate-marketing/#general-editorial-style-guidelines), default to AP style.

- Is it written in [American English](/handbook/marketing/corporate-marketing/#appendix-b-uk-vs-american-english)?
- Are all titles and headlines in [sentence case](/handbook/marketing/corporate-marketing/#capitalization)?
- Has all the relevant [frontmatter](/handbook/marketing/blog/#frontmatter) been included correctly?
- Has the correct [category](/handbook/marketing/blog/#categories) been entered? If not, the build will fail.
- Has `gitlab` been added to the `author_twitter` field if the author doesn't wish to use their own handle?
- Does the `Description` field copy fit on the tile on [/blog](/blog)?
- Is it appropriate to include a [trial CTA](/handbook/marketing/blog/#ee-trial-banner) on this post? Generally, if a post falls under `culture`, `engineering`, or `open source` it is best to remove it.
- Are all [images formatted correctly](/handbook/marketing/blog/#adding-inline-images) and are they <1MB?
- Does any of the images need a [caption](/handbook/marketing/blog/#image-captions)?
- Could we include a [newsletter sign-up form](/handbook/marketing/blog/#newsletter-sign-up-form) anywhere?
- Is there an [image credit](/handbook/marketing/blog/#cover-image-1), if needed?
- Does the click-to-tweet copy make sense? Could it include any other handles or hashtags? [Edit it](/handbook/marketing/blog/#twitter-text) if necessary.
