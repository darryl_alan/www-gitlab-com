---
layout: markdown_page
title: "RM.1.02 - Continuous Monitoring Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# RM.1.02 - Continuous Monitoring

## Control Statement

The design and operating effectiveness of internal controls are continuously evaluated against the established controls framework by GitLab. Corrective actions related to identified deficiencies are tracked to resolution.

## Context

GitLab's controls aim to protect the confidentiality, integrity, and availability of customer, GitLab team member, and partner data and the service provided to them. To ensure the controls remain current and relevant, and they're both being used in the way they were intended and have the expected impact, they should be regularly evaluated and improved when necessary.

## Scope

This control applies to all GitLab internal controls.

## Ownership

GitLab Compliance Team

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.1.02_continuous_monitoring.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.1.02_continuous_monitoring.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.1.02_continuous_monitoring.md).

## Framework Mapping

* ISO
  * A.12.7.1
  * A.18.2.2
  * A.18.2.3
* SOC2 CC
  * CC1.2
  * CC3.2
  * CC3.4
  * CC4.1
  * CC4.2
  * CC5.1
  * CC5.2
